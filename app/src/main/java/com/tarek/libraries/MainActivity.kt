package com.tarek.libraries

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.tarek.animatediconbutton.AnimatedIconButton

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val iconButton: AnimatedIconButton = findViewById(R.id.my_button)

        iconButton.onClicked {
            if (iconButton.iconState == AnimatedIconButton.NOT_FAV)
                Toast.makeText(this, "add to FAV", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "remove from FAV", Toast.LENGTH_SHORT).show()

        }

    }
}
